﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    //parametros para movimentacao do tank
    Transform goal;
    float speed = 5.0f;
    float accuracy = 1.0f;
    float rotSpeed = 2.0f;

    //identifica waypoints para o Tank
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentWP = 0;
    Graph g;
    
    void Start()
    {
        //inicializacao dos parâmetros do WPManager no hierarchy
        wps = wpManager.GetComponent<WPManager>().waypoints;
        g = wpManager.GetComponent<WPManager>().graph;
        currentNode = wps[0];
    }

    //metodos para os botoes da HUD
    public void GoToHeli(){
        g.AStar(currentNode, wps[0]); //envia para o node 0 - heliporto
        currentWP = 0;
    }
    public void GoToRuin(){
        g.AStar(currentNode, wps[5]); //envia para o node 5 - ruinas
        currentWP = 0;
    }
    public void GoToTanks(){
        g.AStar(currentNode, wps[10]); //envia para o node 10 - tanques
        currentWP = 0;
    }

    private void LateUpdate()
    {
        if(g.getPathLength() == 0 || currentWP == g.getPathLength())
        {
           Debug.Log("CHEGOU");
           return; //se terminou o caminho, return vazio 
        }
        

        //pega nó que estará mais próximo neste momento
        currentNode = g.getPathPoint(currentWP);

        //se estivermos mais próximo o bastante do nó o tanque se moverá para o próximo
        if (Vector3.Distance(g.getPathPoint(currentWP).transform.position, transform.position) < accuracy) {
            Debug.LogError("MOVE PARA O MAIS PROXIMO");
            currentWP++;
        }

        //se ainda não chegou no waypoint final do caminho, movimenta o tank usando lookAtGoal para fazer uma cruva
        if (currentWP < g.getPathLength()){ 
            goal = g.getPathPoint(currentWP).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);
        }

        //precisa de movimento neah?
        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
