﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
//gera estrutura de Links
public struct Link
{
    public enum direction {UNI, BI} //enum para gerar o menu dropdown
    public GameObject node1;
    public GameObject node2;
    public direction dir;
}

public class WPManager : MonoBehaviour
{
    public GameObject[] waypoints; //waypoints que estão no mapa
    public Graph graph = new Graph(); //Grap vai desenhar os caminhos dos links
    public Link[] links; //os links criados logo acima

    void Start()
    {
        //pega cada waypoint e adiciona na lista do Graph
        if(waypoints.Length > 0){
            foreach(GameObject wp in waypoints){
                graph.AddNode(wp);
            }
            foreach(Link l in links){ //para cada link desenha o caminho
                graph.AddEdge(l.node1, l.node2); //faz linha unidirecional
                if (l.dir == Link.direction.BI){ //adiciona ponta azul nas duas pontas se for bidirecional
                    graph.AddEdge(l.node2, l.node1);
                }
            }
        }
        
    }

    void Update()
    {
        graph.debugDraw(); //desenha os caminhos na scene
    }
}
